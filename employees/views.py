from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import Employees, Positions
from .forms import EmployeeForm, UpdateEmployeeForm, PositionsForm, UpdatePositionsForm


# Create your views here.
#Create
class AddEmployee(generic.CreateView):
    template_name = "employees/em_create.html"
    model = Employees
    form_class = EmployeeForm
    success_url = reverse_lazy("employees:em_list")

#List
class ListEmployees(generic.View):
    template_name = "employees/em_list.html"
    context = {}

    def get(self, request, *args, **kwargs):
        employeess_list = Employees.objects.all()
        self.context = {
            "employees": employeess_list
        }

        return render(request, self.template_name, self.context)

#Details
class DetailEmployee(generic.DetailView):
    template_name = "employees/em_detail.html"
    model = Employees

#Update
class UpdateEmployee(generic.UpdateView):
    template_name = "employees/em_update.html"
    model = Employees
    form_class = UpdateEmployeeForm
    success_url = reverse_lazy("employees:em_list")

#Delete
class DeleteEmployee(generic.DeleteView):
    template_name = "employees/em_delete.html"
    model = Employees
    success_url = reverse_lazy("employees:em_list")



#Positions
#Create
class AddPosition(generic.CreateView):
    template_name = "employees/pos_create.html"
    model = Positions
    form_class = PositionsForm
    success_url = reverse_lazy("employees:pos_list")

#List
class ListPositions(generic.View):
    template_name = "employees/pos_list.html"
    context = {}

    def get(self, request, *args, **kwargs):
        positions_list = Positions.objects.all()
        self.context = {
            "positions": positions_list
        }

        return render(request, self.template_name, self.context)

#Details
class DetailPositions(generic.DetailView):
    template_name = "employees/pos_detail.html"
    model = Positions

#Update
class UpdatePositions(generic.UpdateView):
    template_name = "employees/pos_update.html"
    model = Positions
    form_class = UpdatePositionsForm
    success_url = reverse_lazy("employees:pos_list")

#Delete
class DeletePositions(generic.DeleteView):
    template_name = "employees/pos_delete.html"
    model = Positions
    success_url = reverse_lazy("employees:pos_list")