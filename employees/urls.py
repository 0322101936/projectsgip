from django.urls import path
from employees import views

app_name = "employees"

urlpatterns = [
    path('list/', views.ListEmployees.as_view(), name="em_list"),
    path('add/', views.AddEmployee.as_view(), name="em_add"),
    path('detail/<int:pk>/', views.DetailEmployee.as_view(), name="em_detail"),
    path('update/<int:pk>/', views.UpdateEmployee.as_view(), name="em_update"),
    path('delete/<int:pk>/', views.DeleteEmployee.as_view(), name="em_delete"),
    path('position/list/', views.ListPositions.as_view(), name="pos_list"),
    path('position/add/', views.AddPosition.as_view(), name="pos_add"),
    path('position/detail/<int:pk>/', views.DetailPositions.as_view(), name="pos_detail"),
    path('position/update/<int:pk>/', views.UpdatePositions.as_view(), name="pos_update"),
    path('position/delete/<int:pk>/', views.DeletePositions.as_view(), name="pos_delete"),
]