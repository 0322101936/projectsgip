from django import forms
from .models import Employees, Positions

class EmployeeForm(forms.ModelForm):
    class Meta:
        model = Employees
        fields = "__all__"
        widgets = {
            "em_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the name of the employee"
                }
            ),
            "em_last_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact name of the employee"
                }
            ),
            "em_middle_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact last name of the employee"
                }
            ),
            "em_position": forms.Select(
                attrs={
                    "type":"select", "class":"form-control"
                }
            )
        }

class UpdateEmployeeForm(forms.ModelForm):
    class Meta:
        model = Employees
        fields = "__all__"
        widgets = {
            "em_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the name of the employee"
                }
            ),
            "em_last_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact name of the employee"
                }
            ),
            "em_middle_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact last name of the employee"
                }
            ),
            "em_position": forms.Select(
                attrs={
                    "type":"select", "class":"form-control"
                }
            )
        }

class PositionsForm(forms.ModelForm):
    class Meta:
        model = Positions
        fields = "__all__"
        widgets = {
            "em_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the cpositionof the employee"
                }
            )
        }

class UpdatePositionsForm(forms.ModelForm):
    class Meta:
        model = Positions
        fields = "__all__"
        widgets = {
            "em_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the cpositionof the employee"
                }
            )
        }