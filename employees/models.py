from django.db import models

# Create your models here.
class Positions(models.Model):
    pos_name = models.CharField(max_length=255)

    def __str__(self):
        return self.pos_name
    
class Employees(models.Model):
    em_name = models.CharField(max_length=55)
    em_last_name = models.CharField(max_length=55)
    em_middle_name = models.CharField(max_length=55)
    em_position = models.ForeignKey('Positions', on_delete=models.CASCADE)

    def __str__(self):
        return self.em_name