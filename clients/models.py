from django.db import models

# Create your models here.
class Clients(models.Model):
    cl_name = models.CharField(max_length=150)
    cl_contact_name = models.CharField(max_length=55)
    cl_contact_lastname = models.CharField(max_length=55)
    cl_contact_middle_name = models.CharField(max_length=55)
    cl_contact_number_phone = models.CharField(max_length=20)

    def __str__(self):
        return self.cl_name