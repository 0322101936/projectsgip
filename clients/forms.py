from django import forms
from .models import Clients

class ClientForm(forms.ModelForm):
    class Meta:
        model = Clients
        fields = "__all__"
        widgets = {
            "cl_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the name of the client"
                }
            ),
            "cl_contact_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact name of the client"
                }
            ),
            "cl_contact_lastname": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact last name of the client"
                }
            ),
            "cl_contact_middle_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact middle name of the client"
                }
            ),
            "cl_contact_number_phone": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact number phone of the client"
                }
            )
        }

class UpdateClientForm(forms.ModelForm):
    class Meta:
        model = Clients
        fields = "__all__"
        widgets = {
            "cl_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the name of the client"
                }
            ),
            "cl_contact_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact name of the client"
                }
            ),
            "cl_contact_lastname": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact last name of the client"
                }
            ),
            "cl_contact_middle_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact middle name of the client"
                }
            ),
            "cl_contact_number_phone": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact number phone of the client"
                }
            )
        }