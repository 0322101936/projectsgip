from django.urls import path
from clients import views

app_name = "clients"

urlpatterns = [
    path('list/', views.ListClients.as_view(), name="cl_list"),
    path('add/', views.AddClient.as_view(), name="cl_add"),
    path('detail/<int:pk>/', views.DetailClient.as_view(), name="cl_detail"),
    path('update/<int:pk>/', views.UpdateClient.as_view(), name="cl_update"),
    path('delete/<int:pk>/', views.DeleteClient.as_view(), name="cl_delete"),

]