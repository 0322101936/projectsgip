from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import Clients
from .forms import ClientForm, UpdateClientForm


# Create your views here.
#Create
class AddClient(generic.CreateView):
    template_name = "clients/cl_create.html"
    model = Clients
    form_class = ClientForm
    success_url = reverse_lazy("clients:cl_list")

#List
class ListClients(generic.View):
    template_name = "clients/cl_list.html"
    context = {}

    def get(self, request, *args, **kwargs):
        client_list = Clients.objects.all()
        self.context = {
            "clients": client_list
        }

        return render(request, self.template_name, self.context)

#Details
class DetailClient(generic.DetailView):
    template_name = "clients/cl_detail.html"
    model = Clients

#Update
class UpdateClient(generic.UpdateView):
    template_name = "clients/cl_update.html"
    model = Clients
    form_class = UpdateClientForm
    success_url = reverse_lazy("clients:cl_list")

#Delete
class DeleteClient(generic.DeleteView):
    template_name = "clients/cl_delete.html"
    model = Clients
    success_url = reverse_lazy("clients:cl_list")

