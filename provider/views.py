from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import Providers
from .forms import ProviderForm, UpdateProviderForm


# Create your views here.
#Create
class AddProvider(generic.CreateView):
    template_name = "providers/pr_create.html"
    model = Providers
    form_class = ProviderForm
    success_url = reverse_lazy("provider:pr_list")

#List
class ListProvider(generic.View):
    template_name = "providers/pr_list.html"
    context = {}

    def get(self, request, *args, **kwargs):
        providers_list = Providers.objects.all()
        self.context = {
            "providers": providers_list
        }

        return render(request, self.template_name, self.context)

#Details
class DetailProvider(generic.DetailView):
    template_name = "providers/pr_detail.html"
    model = Providers

#Update
class UpdateProvider(generic.UpdateView):
    template_name = "providers/pr_update.html"
    model = Providers
    form_class = UpdateProviderForm
    success_url = reverse_lazy("provider:pr_list")

#Delete
class DeleteProvider(generic.DeleteView):
    template_name = "providers/pr_delete.html"
    model = Providers
    success_url = reverse_lazy("provider:pr_list")

