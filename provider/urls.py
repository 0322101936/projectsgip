from django.urls import path
from provider import views

app_name = "provider"

urlpatterns = [
    path('list/', views.ListProvider.as_view(), name="pr_list"),
    path('add/', views.AddProvider.as_view(), name="pr_add"),
    path('detail/<int:pk>/', views.DetailProvider.as_view(), name="pr_detail"),
    path('update/<int:pk>/', views.UpdateProvider.as_view(), name="pr_update"),
    path('delete/<int:pk>/', views.DeleteProvider.as_view(), name="pr_delete"),
]