from django.db import models

# Create your models here.
class Providers(models.Model):
    pr_name = models.CharField(max_length=150)
    pr_contact_name = models.CharField(max_length=55)
    pr_contact_lastname = models.CharField(max_length=55)
    pr_contact_middle_name = models.CharField(max_length=55)
    pr_contact_number_phone = models.CharField(max_length=20)

    def __str__(self):
        return self.pr_name