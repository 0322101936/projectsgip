from django import forms
from .models import Providers

class ProviderForm(forms.ModelForm):
    class Meta:
        model = Providers
        fields = "__all__"
        widgets = {
            "pr_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the name of the client"
                }
            ),
            "pr_contact_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact name of the client"
                }
            ),
            "pr_contact_lastname": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact last name of the client"
                }
            ),
            "pr_contact_middle_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact middle name of the client"
                }
            ),
            "pr_contact_number_phone": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact number phone of the client"
                }
            )
        }

class UpdateProviderForm(forms.ModelForm):
    class Meta:
        model = Providers
        fields = "__all__"
        widgets = {
            "pr_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the name of the client"
                }
            ),
            "pr_contact_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact name of the client"
                }
            ),
            "pr_contact_lastname": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact last name of the client"
                }
            ),
            "pr_contact_middle_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact middle name of the client"
                }
            ),
            "pr_contact_number_phone": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the contact number phone of the client"
                }
            )
        }